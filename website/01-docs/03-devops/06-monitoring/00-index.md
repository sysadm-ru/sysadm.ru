---
layout: page
title: Мониторинг серверов и рабочих станций в Linux
permalink: /devops/linux/monitoring/
---

# Мониторинг серверов и рабочих станций в Linux

### [Check_mk](/devops/linux/monitoring/check-mk/)

### [Zabbix](/devops/linux/monitoring/zabbix/)

### [Nagios](/devops/linux/monitoring/nagios/)

### [Icinga (Форк Nagios)](/devops/linux/monitoring/icinga/)
