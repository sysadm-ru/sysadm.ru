---
layout: page
title: Nagios в Linux
permalink: /devops/linux/monitoring/nagios/
---

# Nagios в Linux


### [Инсталляция Nagios в Ubuntu Linux](/devops/linux/monitoring/nagios/ubuntu/16.04/install/)

### [Конфигурирование Nagios для мониторинга хостов](/devops/linux/monitoring/nagios/ubuntu/16.04/configure/)

<br/>

Если нужно дополнительно получить информацию о загрузке процессора, дисков и т.д., можно использовать:

- NRPE (Nagios Remote Plugin Executor)
- NCPA (Nagios Cross-Platform Agent)


Я пока разбираться не стал.
