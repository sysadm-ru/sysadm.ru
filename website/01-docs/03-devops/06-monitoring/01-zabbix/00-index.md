---
layout: page
title: Zabbix в Linux
permalink: /devops/linux/monitoring/zabbix/
---

# Zabbix в Linux


### [Инсталляция Zabbix 3.X в Ubuntu Linux 16.04 (Xenial)](/devops/linux/monitoring/zabbix/3.x/ubuntu/16.04/install/)

### [Инсталляция Zabbix 2.X в Ubuntu Linux 16.04 (Xenial)](/devops/linux/monitoring/zabbix/2.x/ubuntu/16.04/install/)
