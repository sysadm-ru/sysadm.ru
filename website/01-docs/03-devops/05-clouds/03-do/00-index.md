---
layout: page
title: Digital Ocean (DO)
description: Digital Ocean (DO)
keywords: clouds, digital ocean
permalink: /devops/clouds/do/
---

# Digital Ocean (DO)

### [[Webinar] Using Infrastructure as Code to Build Reproducible Systems with Terraform on DigitalOcean](/devops/clouds/do/terraform/)

### [Creating a Kubernetes Cluster with Digital Ocean and Terraform (возможно понадобится VPN)](https://ponderosa.io/blog/kubernetes/2019/03/13/terraform-cluster-create/)

-   актуальная версия kubernetes сейчас

version = "1.16.6-do.0"
