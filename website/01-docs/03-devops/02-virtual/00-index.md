---
layout: page
title: Виртуализация в Linux
description: Виртуализация в Linux
keywords: devops, linux, virtual
permalink: /devops/linux/virtual/
---

# Виртуализация в Linux

<br/>

### [Virtual Box](/devops/linux/virtual/virtualbox/)

### [KVM на Centos 6](/devops/linux/virtual/kvm/)

### [Proxmox](http://odba.ru/showthread.php?t=351)

<br/>

### Инструменты

### [Vagrant](/devops/linux/virtual/vagrant/)
