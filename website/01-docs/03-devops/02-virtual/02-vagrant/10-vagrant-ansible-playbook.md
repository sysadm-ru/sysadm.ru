---
layout: page
title: Vagrant и Ansible playbook для подготовки виртуальных машин Virtualbox
description: Vagrant и Ansible playbook для подготовки виртуальных машин Virtualbox
keywords: Vagrant и Ansible playbook для подготовки виртуальных машин Virtualbox
permalink: /devops/linux/virtual/vagrant/vagrant-ansible-playbook/
---

# Vagrant и Ansible playbook для подготовки виртуальных машин Virtualbox

<br/>
<br/>

![Vagrant и Ansible playbook](/img/devops/linux/virtual/vagrant/ansible/pic1.png 'Vagrant и Ansible playbook')

<br/>
<br/>

![Vagrant и Ansible playbook](/img/devops/linux/virtual/vagrant/ansible/pic2.png 'Vagrant и Ansible playbook')
