---
layout: page
title: Vagrant в Linux
description: Vagrant в Linux
keywords: linux, ubuntu, vagrant
permalink: /devops/linux/virtual/vagrant/
---

# Vagrant в Linux

Я vagrant использую, когда мне нужна виртуалка и мне впадлу инсталлировать ее вручную.

Разумеется можно сделать образы и при необходимости их можно каждый раз импортировать, но раз есть инструмент, который с помощью одной команды может поднимать одну или несколько виртуальных машин, почему бы его не использовать.

<br/>

### Наработки админа сайта

[Инсталляция Vargant в Ubuntu 20.04.1](/devops/linux/virtual/vagrant/setup/ubuntu/)

[Знакомство с Vagrant](/devops/linux/virtual/vagrant/crash-course/)

[Команды Vagrant](/devops/linux/virtual/vagrant/commands/)

[SSH keys](/devops/linux/virtual/vagrant/ssh-keygen/)

[Создание с помощью Vagrant виртуальной машины Ubuntu](/devops/linux/virtual/vagrant/create-ubuntu-vm-by-vagrant/)

[Создание с помощью Vagrant 2-х виртуальных машин Debian](/devops/linux/virtual/vagrant/create-2-debian-vagrant/)

[Vagrant c Docker внутри](/devops/linux/virtual/vagrant/vagrant-with-docker/)

[Деплой nodejs приложения на удаленный сервер с помощью ansible](/devops/automation/ansible/deploy-node-app-by-ansible/)

[Vagrant и Ansible playbook для подготовки виртуальных машин Virtualbox](/devops/linux/virtual/vagrant/vagrant-ansible-playbook/)

[Разворачиваем Gitlab в виртуальной машине Vagrant подготовленными скриптами](/devops/gitops/cvs/gitlab/vagrant/)

[Скрипты, разворачивающие Single Master Kubernetes Cluster в VirtualBox](https://github.com/webmakaka/vagrant-kubernetes-3-node-cluster-centos7)

[Скрипты для развертывания Kubernetes с помощью Ansible в Vagrant](https://bitbucket.org/sysadm-ru/vagrant-ansible-kubernetes/)
