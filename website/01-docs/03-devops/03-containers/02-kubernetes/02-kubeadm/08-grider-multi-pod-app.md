---
layout: page
title: Разворачиваем приложение из видео курса Stephen Grider Docker and Kubernetes The Complete Guide
description: Разворачиваем приложение из видео курса Stephen Grider Docker and Kubernetes The Complete Guide
keywords: devops, linux, kubernetes, Разворачиваем приложение из видео курса Stephen Grider Docker and Kubernetes The Complete Guide
permalink: /devops/containers/kubernetes/kubeadm/grider-multi-pod-app/
---

# Разворачиваем приложение из видео курса [Stephen Grider] Docker and Kubernetes: The Complete Guide [2019, ENG]

<br/>

Все перенесено на <a href="https://github.com/webmak1/Docker-and-Kubernetes-The-Complete-Guide-Deploy-on-Local-Kubernetes-Cluster-Only">github</a>
