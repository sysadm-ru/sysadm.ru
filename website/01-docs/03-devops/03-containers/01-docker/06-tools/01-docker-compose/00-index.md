---
layout: page
title: Инсталляция Docker-Compose (для совместной работы контейнеров)
description: Инсталляция Docker-Compose (для совместной работы контейнеров)
keywords: devops, docker, Инсталляция Docker-Compose (для совместной работы контейнеров)
permalink: /devops/containers/docker/tools/docker-compose/
---

# Инсталляция Docker-Compose (для совместной работы контейнеров)

<br/>

[Инсталляция Docker-Compose в Ubuntu 18.04 ](/devops/containers/docker/setup/ubuntu/)

[Линковка Docker контейнеров с помощью Docker Compose](/devops/containers/docker/tools/docker-compose/linking/)
