---
layout: page
title: Обучающие видео по работе с Ansible
description: Обучающие видео по работе с Ansible
keywords: Обучающие видео по работе с Ansible
permalink: /devops/automation/ansible/videos/
---

# Обучающие видео по работе с Ansible

<br/>

### Ansible На Русском Языке

https://www.youtube.com/playlist?list=PLg5SS_4L6LYufspdPupdynbMQTBnZd31N

<br/>

### Автоматизация администрирования. Ansible. (Linux-2018-07)

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/IBWO1Zk37UU" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

### Introduction to Ansible

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/iVWmbStE1MM" frameborder="0" allowfullscreen></iframe>
</div>
