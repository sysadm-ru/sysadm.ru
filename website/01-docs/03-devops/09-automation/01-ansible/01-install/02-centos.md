---
layout: page
title: Инсталляция Ansible в Centos 7.3
description: Инсталляция Ansible в Centos 7.3
keywords: Инсталляция Ansible в Centos 7.3
permalink: /devops/automation/ansible/install/centos/
---

# Инсталляция Ansible в Centos 7.3

    $ su -
    # usermod -aG wheel user

    # yum install -y python2 epel-release

    # yum install -y ansible
