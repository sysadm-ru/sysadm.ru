---
layout: page
title: Инсталляция Ansible
description: Инсталляция Ansible
keywords: Инсталляция Ansible
permalink: /devops/automation/ansible/install/
---

# Инсталляция Ansible

<br/>

### [Инсталляция Ansible в Ubuntu 18.04](/devops/automation/ansible/install/ubuntu/)

### [Инсталляция Ansible в Centos 7.3](/devops/automation/ansible/install/centos/)
