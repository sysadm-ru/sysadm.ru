---
layout: page
title: Автоматизация Ansible, Puppet, Chef, Terraform
description: Автоматизация Ansible, Puppet, Chef, Terraform
keywords: DevOps, Автоматизация Ansible, Puppet, Chef, Terraform
permalink: /devops/automation/
---

# Автоматизация Ansible, Puppet, Chef, Terraform

<br/>

### [Ansible](/devops/automation/ansible/)

<br/>

### [Puppet](/devops/automation/puppet/)

<br/>

### Chef

<br/>

### [Terraform](/devops/automation/terraform/)
