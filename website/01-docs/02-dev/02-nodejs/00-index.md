---
layout: page
title: Программирование в Linux Node.JS
description: Программирование в Linux Node.JS
keywords: dev, nodejs, Программирование в Linux Node.JS
permalink: /dev/nodejs/
---

# Программирование в Linux Node.JS

<a href="/dev/nodejs/installation/centos/">Инсталляция node.js, bower в centos 6.X</a>

<br/>

### How to Install Node.js In Ubuntu 15.10

[Brad Traversy, YouTube](https://www.youtube.com/watch?v=AcUfdajsKg8)

<br/>

### Node.js Production

[How To Set Up a Node.js Application for Production on Ubuntu 16.04](/dev/nodejs/production/)
