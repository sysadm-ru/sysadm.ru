---
layout: page
title: Программирование в Linux
description: Программирование в Linux
keywords: Программирование в Linux
permalink: /dev/
---

# Программирование в Linux

### [Текстовый редактор в Linux с подсветкой синтаксиса и парных тегов HTML (аналог notepad++ в Windows)](/desktop/linux/code/editors/)

### [GIT](/dev/git/)

### [JAVA](//javadev.org/devtools/jdk/setup/)

### [GO](/dev/go/)

### [Node.JS](/dev/nodejs/)

### [Ruby on Rails](/dev/ruby-on-rails/)
