---
layout: page
title: Инсталляция GIT из исходников
description: Инсталляция GIT из исходников
keywords: Инсталляция GIT из исходников
permalink: /dev/git/install/
---

# Инсталляция GIT из исходников

### [Ubuntu](/dev/git/install/ubuntu/)

### [Centos](/dev/git/install/centos/)

### [Ошибки при работе с GIT](/dev/git/errors/)
