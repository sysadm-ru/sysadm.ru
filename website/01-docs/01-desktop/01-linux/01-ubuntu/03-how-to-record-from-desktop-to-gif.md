---
layout: page
title: Запись с экрана монитора в GIF
description: Запись с экрана монитора в GIF
keywords: linux, ubuntu, gif, запись, peek
permalink: /desktop/linux/ubuntu/how-to-record-from-desktop-to-gif/
---

# Запись с экрана монитора в GIF

<br/>


    $ sudo add-apt-repository ppa:peek-developers/stable
    $ sudo apt-get update
    $ sudo apt-get install peek

    $ peek