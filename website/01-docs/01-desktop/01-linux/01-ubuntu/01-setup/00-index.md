---
layout: page
title: Шаги после инсталляции Ubuntu
description: Шаги после инсталляции Ubuntu
keywords: Шаги после инсталляции Ubuntu
permalink: /desktop/linux/ubuntu/setup/
---

# Шаги после инсталляции Ubuntu

<br/>

<ul>
    <li><a href="/desktop/linux/ubuntu/setup/steps-after-installation-ubuntu-20.04-lts/">Шаги после инсталляции Ubuntu 20.04 LTS</a>
    </li>

<!--

    <li><a href="/desktop/linux/ubuntu/setup/steps-after-installation-ubuntu-14/">Шаги после инсталляции Ubuntu 14</a>
    </li>

-->

</ul>
