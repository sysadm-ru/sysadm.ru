---
layout: page
title: Базы Данных
description: Базы Данных
keywords: Базы Данных
permalink: /adm/databases/
---

# Базы Данных

<br/>

**Реляционные**

[MySQL](/databases/mysql/)  
[PostgreSQL](/databases/postgresql/)  
[Oracle](https://oracle-dba.ru/database/installation/)

<br/>

**Нереляционные**

[MongoDB](/databases/mongodb/)  
[Cassandra](/databases/cassandra/centos/6.7/)

<br/>

**Кластера**

[Percona XtraDB (MySQL)](/databases/percona/)
