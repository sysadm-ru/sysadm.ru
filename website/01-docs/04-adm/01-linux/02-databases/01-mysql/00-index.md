---
layout: page
title: MySQL
description: MySQL
keywords: MySQL
permalink: /databases/mysql/
---

# MySQL

<br/>

[Инсталляция MySQL на Centos 7 из пакетов](/databases/mysql/installation/centos7/)

[Инсталляция MySQL на Centos 6.5 из пакетов](/databases/mysql/installation/)

[Конфигурирование пользователей после инсталляции](/databases/mysql/installation/users/)

[Удаленный доступ к базе root'ом с правами суперпользователя](/databases/mysql/root-connection/)

[Improving local file security](/databases/mysql/installation/security/)

<br/>

## Логи MySQL

    $ tail /var/log/mysqld.log

<br/>

### Команды для работы с базой данных MySQL

[Команды для работы с базой данных MySQL](/databases/mysql/commands/)

[mysqldump](/databases/mysql/mysqldump/)
